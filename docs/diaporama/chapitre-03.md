<!-- .slide: data-background-image="images/ws-bg.png" data-background-size="600px" class="chapter" -->

## La construction de l'API

%%%

<!-- .slide: data-background-image="images/ws-bg.png" data-background-size="300px" class="slide" -->

### Application Programming Interface

<p>Que nous dit wikipedia ?</p>
	<blockquote class="fragment" cite="https://fr.wikipedia.org/wiki/Interface_de_programmation">
	En informatique, une interface de programmation applicative
						(souvent désignée par le terme API pour application programming
						interface) est un ensemble normalisé de classes, de méthodes ou
						de fonctions qui sert de façade par laquelle un logiciel offre
						des services à d'autres logiciels.
	</blockquote>
</p>


%%%

<!-- .slide: data-background-image="images/ws-bg.png" data-background-size="300px" class="slide" -->
### Dit autrement

Une Brique applicative qui offre un service, et qu'on utilise sans savoir comment elle fonctionne.

2 types
- Les bibliothèques logicielles
- Les web services

Remarque: 
    -  Vous risquez d'utiliser les deux




%%%
<!-- .slide: data-background-image="images/ws-bg.png" data-background-size="300px" class="slide" -->

### Exemple d'une requête sur une api 

[**thecocktaildb**](https://www.thecocktaildb.com/api.php?ref=apilist.fun) est une API qui recense de nombreuses informations sur la confection de cocktails.

On peut par exemple recuperer les informations sur la réalisation du **cuba libre**

- ici : https://www.thecocktaildb.com/api/json/v1/1/search.php?s=cuba%20libre

<div style="overflow:scroll; height:400px;">


<pre id="json"><code>
{
   "drinks":[
      {
         "idDrink":"11288",
         "strDrink":"Cuba Libre",
         "strDrinkAlternate":null,
         "strTags":"IBA,ContemporaryClassic",
         "strVideo":null,
         "strCategory":"Ordinary Drink",
         "strIBA":"Contemporary Classics",
         "strAlcoholic":"Alcoholic",
         "strGlass":"Highball glass",
         "strInstructions":"Build all ingredients in a Collins glass filled with ice. Garnish with lime wedge.",
         "strInstructionsES":null,
         "strInstructionsDE":"Geben Sie alle Zutaten in ein mit Eis gef\u00fclltes Collins-Glas. Mit Limettenkeil garnieren.",
         "strInstructionsFR":null,
         "strInstructionsIT":"Mettere tutti gli ingredienti in un bicchiere Collins pieno di ghiaccio. Guarnire con uno spicchio di lime.",
         "strInstructionsZH-HANS":null,
         "strInstructionsZH-HANT":null,
         "strDrinkThumb":"https:\/\/www.thecocktaildb.com\/images\/media\/drink\/wmkbfj1606853905.jpg",
         "strIngredient1":"Light rum",
         "strIngredient2":"Lime",
         "strIngredient3":"Coca-Cola",
         "strIngredient4":null,
         "strIngredient5":null,
         "strIngredient6":null,
         "strIngredient7":null,
         "strIngredient8":null,
         "strIngredient9":null,
         "strIngredient10":null,
         "strIngredient11":null,
         "strIngredient12":null,
         "strIngredient13":null,
         "strIngredient14":null,
         "strIngredient15":null,
         "strMeasure1":"2 oz ",
         "strMeasure2":"Juice of 1\/2 ",
         "strMeasure3":null,
         "strMeasure4":null,
         "strMeasure5":null,
         "strMeasure6":null,
         "strMeasure7":null,
         "strMeasure8":null,
         "strMeasure9":null,
         "strMeasure10":null,
         "strMeasure11":null,
         "strMeasure12":null,
         "strMeasure13":null,
         "strMeasure14":null,
         "strMeasure15":null,
         "strImageSource":"https:\/\/commons.wikimedia.org\/wiki\/File:Cuba_Libre.jpg",
         "strImageAttribution":"Martin Belam (currybet)",
         "strCreativeCommonsConfirmed":"Yes",
         "dateModified":"2016-09-06 23:41:03"
      }
   ]
}
</code></pre>
</div>

%%%

<!-- .slide: data-background-image="images/ws-bg.png" data-background-size="300px" class="slide" -->

### Du JSON, vraiment ?

Oui. 

<blockquote class="fragment">JSON : JavaScript Object Notation <br/> Un autre format très courant du web avec le xml, yaml..</a></blockquote>
