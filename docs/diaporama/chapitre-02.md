<!-- .slide: data-background-image="images/datageneration.png" data-background-size="600px" class="chapter" -->

## Rappel du besoin

%%%

<!-- .slide: data-background-image="images/datageneration.png" data-background-size="300px" class="slide"   -->

### Contexte

A partir du moment où l'on utilise des services en ligne, HTTP étant la base du web, l'on peut désirer générer des interfaces pour permettre la simulation de comportements connus.

Par exemple, lorsque vous allez sur google ou instagram, vous échangez des jsons qui sont rendus sur vos appareils par requêtes HTTP.

Dans ce contexte l'idée est de produire un webservice pouvant s'interfacer pour produire des modèles de données équivalents à ceux que l'on peut retrouver, pour s'interfacer dans le cadre de tests ou de maquettes.

%%%

<!-- .slide: data-background-image="images/datageneration.png" data-background-size="300px"  class="slide" -->

### Fonctionnalités de base attendues

- Définition des **valeurs** possibles pour un champ 
- Définition d'un **schéma** correspondant a une ligne de données à générer.
- Définition d'un taux de remplissage pour un champ.
- Démarrage de l'applicatif à partir d'un fichier de règles de bases
- La réalisation d'un scénario d'usage de l'API.

%%%

<!-- .slide: data-background-image="images/datageneration.png" data-background-size="300px"  class="slide" -->

### Fonctionnalités avancées

Au moins une est exigée 

- Génération d'un jeu de règles à partir d'un jeu de données
- Gestion de l'authentification à l'API
- Permettre l'export et l'import de schémas sous format JSON
- Proposer une solution répondant à des enjeux de volumétrie (taille des jeux de données)
- Réalisation de rapports de statistiques sur les jeux générés.


%%%
<!-- .slide: data-background-image="images/datageneration.png" data-background-size="300px"  class="slide" -->

### C'est votre application

Soyez originaux et n'hésitez pas a me contacter si vous voulez vous aventurer hors des sentiers battus ! :)

